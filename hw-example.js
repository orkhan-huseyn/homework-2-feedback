
const userForm = document.getElementById('user-form');
const userList = document.getElementById('user-list');
const countSpan = document.getElementById('count');

const users = [];

userForm.addEventListener('submit', function(event) {
  event.preventDefault();
  const { username, type } = userForm.elements;

  if (!username.value) {
    return;
  }

  users.push({
    username: username.value,
    type: type.value,
  });

  displayList(users);
  updateUserCount(users);

  username.value = '';
});

userList.addEventListener('click', function(event) {
  if (event.target.tagName === 'BUTTON') {
    const index = Number(event.target.parentElement.id);
    users.splice(index, 1);
    displayList(users);
    updateUserCount(users);
  }

  if (event.target.tagName === 'INPUT') {
    const checkbox = event.target;
    const text = checkbox.nextElementSibling.firstElementChild;
    if (checkbox.checked) {
      text.style.textDecoration = 'line-through';
    } else {
      text.style.textDecoration = 'none';
    }
  }
});

function displayList(users) {
  let usersHTML = '';
  let index = 0;
  for (let user of users) {
    usersHTML += `
      <li id="${index}" class="list-group-item d-flex justify-content-between align-items-start">
        <input type="checkbox" />
        <div class="ms-2 me-auto">
          <div class="fw-bold">${user.username}</div>
          <span class="badge bg-primary rounded-pill">${user.type}</span>
        </div>
        <button type="button" class="btn btn-danger btn-sm">Delete</button>
      </li>
    `;
    index++;
  }
  userList.innerHTML = usersHTML;
}

function updateUserCount(users) {
  countSpan.textContent = users.length;
}
