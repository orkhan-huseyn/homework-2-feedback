
function random(min, max) {
  return Math.floor(Math.random() * max + min);
}

function pickOne(arr) {
  const randomIndex = random(0, arr.length);
  return arr[randomIndex];
}

function includes(arr, target) {
  for (let element of arr) {
    if (element === target) {
      return true;
    }
  }
  return false;
}

function unique(arr) {
  const uniqueArr = [];

  for (let i = 0; i < arr.length; i++) {
    let current = arr[i];
    let restOfTheArray = arr.slice(i + 1);
    // if rest of the array does not include current
    if (!includes(restOfTheArray, current)) {
      uniqueArr.push(current);
    }
  }

  return uniqueArr;
}


function union(arr1, arr2) {
  const combined = arr1.concat(arr2);
  return unique(combined);
}


function map(arr, callback) {
  const result = [];
  for (let i = 0; i < arr.length; i++) {
    result.push(callback(arr[i], i, arr));
  }
  return result;
}



function filter(arr, callback) {
  const result = [];
  for (let i = 0; i < arr.length; i++) {
    if (callback(arr[i], i, arr) ===  true) {
      result.push(arr[i]);
    }
  }
  return result;
}


function some(arr, callback) {
  for (let i = 0; i < arr.length; i++) {
    if (callback(arr[i], i, arr) ===  true) {
      return true;
    }
  }
  return false;
}

function every(arr, callback) {
  for (let i = 0; i < arr.length; i++) {
    if (callback(arr[i], i, arr) === false) {
      return false;
    }
  }
  return true;
}


function reduce(arr, callback, initialValue) {
  let accumulator = initialValue;
  for (let i = 0; i < arr.length; i++) {
    accumulator = callback(accumulator, arr[i], i, arr);
  }
  return accumulator;
}





function generatePassword(passwordLength) {
  const alphabet = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
  let password = '';

  for (let i = 0; i < passwordLength; i++) {
    const randomIndex = random(0, alphabet.length);
    const randomCharacter = alphabet.charAt(randomIndex);
    password += randomCharacter;
  }

  return password;
}


function ceasarCipher(string, k) {
  let decrypted = '';

  for (let char of string) {
    const charCode = char.charCodeAt(0);
    if (isLowerCaseLetter(char)) {
      let nextCode = charCode + k;
      if (nextCode > 122) {
        nextCode = nextCode - 26;
      }
      const decodedLetter = String.fromCharCode(nextCode);
      decrypted += decodedLetter;
    } else if (isUpperCaseLetter(char)) {
      let nextCode = charCode + k;
      if (nextCode > 90) {
        nextCode = nextCode - 26;
      }
      const decodedLetter = String.fromCharCode(nextCode);
      decrypted += decodedLetter;
    } else {
      decrypted += char;
    }
  }

  return decrypted;
}


function isLowerCaseLetter(char) {
  const charCode = char.charCodeAt(0);
  return charCode >= 97 && charCode <= 122;
}


function isUpperCaseLetter(char) {
  const charCode = char.charCodeAt(0);
  return charCode >= 65 && charCode <= 90;
}





//
